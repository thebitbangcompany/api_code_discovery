import sys
import csv
import random as rnd
sys.path.append('./')
from models import Segment, Family, _Class, MaterialType, Product
from tqdm import tqdm

if __name__ == '__main__':

    with open('./clasificador_de_bienes_y_servicios.csv', 'rt', encoding='latin-1') as csvfile:
        reader = csv.DictReader(csvfile, delimiter=';')
        row_count = sum(1 for row in reader)
        csvfile.seek(0)
        reader = csv.DictReader(csvfile, delimiter=';')
        with tqdm(total=row_count) as progress_bar:
            for row in tqdm(reader):
                segment = Segment(name=row['segment_name'], code=row['segment_code'])
                family = Family(name=row['family_name'], code=row['family_code'], segment=segment)
                _class = _Class(name=row['class_name'], code=row['class_code'], family=family)
                material_type = MaterialType(name=row['material_type_name'], code=row['material_type_code'], _class=_class)
                trial_product = Product(name=f"{row['material_type_name']} {rnd.randint(1,999999)}", description=row['material_type_name'], code=row['material_type_code'], material_type=material_type)

                segment.save()
                family.save()
                _class.save()
                material_type.save()
                trial_product.save()
                
                progress_bar.update(1)