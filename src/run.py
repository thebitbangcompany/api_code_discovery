import os
import sys
sys.path.append('./')
from flask import Flask
from flask_mongoalchemy import MongoAlchemy

app = Flask(__name__)
app.config['MONGOALCHEMY_DATABASE'] = os.getenv('MDB_DATABASE')
app.config['MONGOALCHEMY_SERVER'] = os.getenv('MDB_SERVER')
app.config['MONGOALCHEMY_PORT'] = os.getenv('MDB_PORT')
db = MongoAlchemy(app)

def create_app():

    from app import api_bp
    app.register_blueprint(api_bp, url_prefix=os.getenv('API_URL_PREFIX'))

    return app, db


if __name__ == "__main__":
    app,db = create_app()
    app.run(debug=True, host='0.0.0.0')
