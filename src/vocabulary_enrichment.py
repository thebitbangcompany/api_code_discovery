# -*- coding: utf-8 -*-
import urllib.request
from bs4 import BeautifulSoup as soup
import re
import logging


class VocabularyEnrichment(object):
	"""Load data from excel file"""
	def __init__(self):
		super(VocabularyEnrichment, self).__init__()
		logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
		#self.load()


	def get_synonyms(self, word):
		word = word.lower()
		data = str(urllib.request.urlopen('https://educalingo.com/en/dic-es/{}'.format(word)).read())
		#final_results = re.findall('\w+', [i.text for i in soup(data, 'lxml').find_all('div', {"class":'contenido_sinonimos_antonimos'})][0])
		synonyms = []
		s = soup(data, 'lxml').find_all('div', {"class":'contenido_sinonimos_antonimos'})[0]
		for a in s.find_all('a'):
  			synonyms.append(a.string)
		return synonyms

if __name__ == '__main__':
	ve = VocabularyEnrichment()
	synonyms = ve.get_synonyms('Gato')
	print(synonyms)
