# API code discovery template
Created by TBBC

## Use

* Flask
* Flask-MongoAlchemy

## Set DB
- Execute test_mongo.py in order to add record to MongoDB

## Install

Please create this *requirements.txt* file (I didn't do it because I already had Python3 packages installed)
```bash
pip install -r requirements.txt
```

## Run

```bash
python3 run.py
```

## Run with Docker

```
Before running the following commands make sure of placing the 'clasificador_de_bienes_y_servicios.csv' file in the project directory
```

Run de commands below:

```bash
$ docker-compose build
$ docker-compose up
```
You should get an output similar to the following:

```bash
Creating network "api_code_discovery_default" with the default driver
Creating mongodb ... done
Creating api_code_discovery ... done
Attaching to mongodb, api_code_discovery
api_code_discovery    |  * Serving Flask app "run" (lazy loading)
api_code_discovery    |  * Environment: production
api_code_discovery    |    WARNING: This is a development server. Do not use it in a production deployment.
api_code_discovery    |    Use a production WSGI server instead.
api_code_discovery    |  * Debug mode: on
api_code_discovery    |  * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
api_code_discovery    |  * Restarting with stat
api_code_discovery    |  * Debugger is active!
api_code_discovery    |  * Debugger PIN: XXX-XXX-XX
```

To access the mongo shell run in the terminal:

```bash
$ docker exec -it mongodb mongo
``` 

## Setup (with Docker)

* Enter the `api_code_discovery` container by running the command below:

```bash
docker exec -it api_code_discovery /bin/bash
```

* Once inside the container run the `mongo_import.py` script to load the information in the `clasificador_de_bienes_y_servicios.csv` file to `MongoDB`:

```bash
python mongo_import.py
```

* Now in another terminal enter the `mongodb` container (using `docker exec -it mongodb mongo`) and run the commands below for creating the indexes that enable full text search:

```javascript
use codex;

db.Product.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.MaterialType.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.getCollection("_Class").createIndex( //https://stackoverflow.com/q/24309685/2412831
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.Family.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);

db.Segment.createIndex(
    { "$**" : "text" },
    { default_language: "spanish" }
);
```

* Finally, back in the `api_code_discovery` container run the `test_mongo.py` script.

## test
```bash
curl -X POST \
  http://127.0.0.1:5000/bizbuy/api/1.0/retrieval_code \
  -H 'Accept: */*' \
  -H 'Connection: keep-alive' \
  -H 'Content-Type: application/json' \
  -H 'Host: 127.0.0.1:5000' \
  -d '{
	"query":"\"Gato 1\""
}'
```